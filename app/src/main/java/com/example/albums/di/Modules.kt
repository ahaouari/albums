package com.example.albums.di

import com.example.albums.repositories.AlbumsRepository
import com.example.albums.viewmodels.GalleryViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single { AlbumsRepository() }
    viewModel { GalleryViewModel(get()) }
}