package com.example.albums.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.albums.repositories.AlbumsRepository
import com.example.albums.wrapper.GalleryWrapper

class GalleryViewModel(private val repo: AlbumsRepository) : ViewModel() {

    private var mGallery: MutableLiveData<GalleryWrapper>

    init {
        mGallery = repo.init()
    }

    val gallery: LiveData<GalleryWrapper>?
        get() = mGallery

    fun reload(maxPhotoByAlbum: Int) {
        mGallery = repo.loadGallery(maxPhotoByAlbum)
    }

}
