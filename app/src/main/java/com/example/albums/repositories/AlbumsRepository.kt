package com.example.albums.repositories

import androidx.lifecycle.MutableLiveData
import com.example.albums.services.AlbumsService
import com.example.albums.wrapper.AlbumItemWrapper
import com.example.albums.wrapper.BaseGalleryItemWrapper
import com.example.albums.wrapper.GalleryWrapper
import com.example.albums.wrapper.PhotoItemWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


class AlbumsRepository {

    private val galleryData = MutableLiveData<GalleryWrapper>()

    private var disposable: Disposable? = null

    private var albumsService: AlbumsService

    init {
        val retrofit = Retrofit.Builder().baseUrl("https://jsonplaceholder.typicode.com/")
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()
            )
            .addConverterFactory(GsonConverterFactory.create()).build()

        albumsService = retrofit.create(AlbumsService::class.java)
    }

    fun init(): MutableLiveData<GalleryWrapper> {
        galleryData.value = GalleryWrapper()
        return galleryData
    }

    fun loadGallery(maxPhotoByAlbum: Int): MutableLiveData<GalleryWrapper> {

        if (disposable != null && !disposable!!.isDisposed) {
            disposable!!.dispose()
        }

        val albumsObservable = albumsService.getAlbums
        val photosObservable = albumsService.getPhotos
        var mapAlbumIdTitle: Map<Int, String?>? = null

        val allItems: MutableList<BaseGalleryItemWrapper> = mutableListOf()

        disposable = albumsObservable
            .map { albumItem ->
                // store a map (albumId to albumTitle) will be user later
                mapAlbumIdTitle = albumItem.map { it.id to it.title }.toMap()
            }
            .flatMap { photosObservable }
            .map { photoList ->
                // group photo list by album id
                photoList.groupBy { it.albumId }.forEach { (albumIdKey, listPhotos) ->
                    val albumTitle = mapAlbumIdTitle!!.getValue(albumIdKey)
                    // add an Album Item to allItems
                    allItems.add(AlbumItemWrapper(albumTitle!!))
                    // limit the list size to  maxPhotoByAlbum items
                    listPhotos.subList(0, Math.min(listPhotos.size, maxPhotoByAlbum))
                        // iterate on subList oh photo and add them to allItems
                        .forEach { photoItem -> allItems.add(PhotoItemWrapper(photoItem.thumbnailUrl!!)) }
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                galleryData.value = GalleryWrapper(true)
            }
            .subscribe(
                {
                    galleryData.value = GalleryWrapper(allItems)
                },
                { error ->
                    galleryData.value = GalleryWrapper(error)
                }
            )
        return galleryData
    }

}