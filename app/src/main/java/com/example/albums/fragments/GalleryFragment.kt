package com.example.albums.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.albums.R
import com.example.albums.adapters.PhotoGalleryAdapter
import com.example.albums.viewmodels.GalleryViewModel
import com.example.albums.wrapper.BaseGalleryItemWrapper
import kotlinx.android.synthetic.main.gallery_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel


class GalleryFragment : Fragment() {

    companion object {
        private const val COLUMNS_COUNT_PARAM = "COLUMNS_COUNT_PARAM"
        private const val MAX_PHOTO_BY_ALBUM_PARAM = "MAX_PHOTO_BY_ALBUM_PARAM"

        private const val DEFAULT_COLUMNS_COUNT = 3
        private const val DEFAULT_MAX_PHOTO_BY_ALBUM = 3

        fun newInstance(columnsCount: Int, maxPhotoByAlbum: Int) = GalleryFragment().apply {
            arguments = Bundle().apply {
                putInt(COLUMNS_COUNT_PARAM, columnsCount)
                putInt(MAX_PHOTO_BY_ALBUM_PARAM, maxPhotoByAlbum)
            }
        }
    }

    private var maxPhotoByAlbum: Int = DEFAULT_MAX_PHOTO_BY_ALBUM
    private var columnsCount: Int = DEFAULT_COLUMNS_COUNT

    //private lateinit var galleryViewModel: GalleryViewModel
    // Lazy Inject ViewModel
    private val galleryViewModel: GalleryViewModel by viewModel()

    private lateinit var photoGalleryAdapter: PhotoGalleryAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.gallery_fragment, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        arguments?.getInt(MAX_PHOTO_BY_ALBUM_PARAM)?.let {
            // ignore the MAX_PHOTO_BY_ALBUM_PARAM when less or equals to zero
            if (it > 0) {
                maxPhotoByAlbum = it
            }
        }
        arguments?.getInt(COLUMNS_COUNT_PARAM)?.let {
            // ignore the COLUMNS_COUNT_PARAM when less or equals to zero
            if (it > 0) {
                columnsCount = it
            }
        }
        initList()
        initSwipeToRefresh()
        observeViewModel()
        reloadData()
    }

    private fun initList() {
        photoGalleryAdapter = PhotoGalleryAdapter(arrayListOf())
        val gridLayoutManager = GridLayoutManager(context!!, columnsCount)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

            override fun getSpanSize(position: Int): Int {
                return when (photoGalleryList.adapter!!.getItemViewType(position)) {
                    BaseGalleryItemWrapper.ItemType.ALBUM_TITLE.ordinal -> columnsCount
                    else -> 1
                }
            }
        }
        photoGalleryList.layoutManager = gridLayoutManager
        photoGalleryList.adapter = photoGalleryAdapter
    }

    private fun initSwipeToRefresh() {
        swipeRefresh.setOnRefreshListener { reloadData() }
    }

    private fun reloadData() {
        galleryViewModel.reload(maxPhotoByAlbum)
    }

    private fun observeViewModel() {
        galleryViewModel.gallery?.observe(this, Observer { gallery ->
            when {

                gallery?.isLoading!! -> swipeRefresh.isRefreshing = true

                gallery.throwable == null -> {
                    photoGalleryAdapter.items = gallery.items!!
                    photoGalleryAdapter.notifyDataSetChanged()
                    swipeRefresh.isRefreshing = false
                }

                else -> {
                    Toast.makeText(context, gallery.throwable?.message, Toast.LENGTH_SHORT).show()
                    photoGalleryAdapter.items = arrayListOf()
                    gallery.throwable = null
                    swipeRefresh.isRefreshing = false
                }
            }
        })
    }

}
