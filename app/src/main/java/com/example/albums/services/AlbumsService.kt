package com.example.albums.services

import com.example.albums.model.Album
import com.example.albums.model.Photo
import io.reactivex.Observable
import retrofit2.http.GET

interface AlbumsService {

    @get:GET("albums")
    val getAlbums: Observable<List<Album>>

    @get:GET("photos")
    val getPhotos: Observable<List<Photo>>

}