package com.example.albums.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.albums.R
import com.example.albums.fragments.GalleryFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (supportFragmentManager.findFragmentById(R.id.galleryFragmentContainer) == null) {
            val galleryFragment = GalleryFragment.newInstance(columnsCount = 3, maxPhotoByAlbum = 3)
            supportFragmentManager.beginTransaction().add(R.id.galleryFragmentContainer, galleryFragment).commit()
        }
    }

}