package com.example.albums.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.albums.adapters.holders.AlbumTitleViewHolder
import com.example.albums.adapters.holders.PhotoThumbViewHolder
import com.example.albums.wrapper.AlbumItemWrapper
import com.example.albums.wrapper.BaseGalleryItemWrapper
import com.example.albums.wrapper.PhotoItemWrapper

class PhotoGalleryAdapter(var items: List<BaseGalleryItemWrapper>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            BaseGalleryItemWrapper.ItemType.ALBUM_TITLE.ordinal -> AlbumTitleViewHolder(parent)
            BaseGalleryItemWrapper.ItemType.PHOTO_THUMB.ordinal -> PhotoThumbViewHolder(parent)
            else -> object : RecyclerView.ViewHolder(parent) {}
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AlbumTitleViewHolder -> holder.bindAlbum(items[position] as AlbumItemWrapper)
            is PhotoThumbViewHolder -> holder.bindPhoto(items[position] as PhotoItemWrapper)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].itemType.ordinal
    }

}

