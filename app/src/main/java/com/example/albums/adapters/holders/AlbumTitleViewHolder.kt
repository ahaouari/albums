package com.example.albums.adapters.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.albums.R
import com.example.albums.wrapper.AlbumItemWrapper
import kotlinx.android.synthetic.main.album_title_item.view.*

class AlbumTitleViewHolder(parent: ViewGroup) :
    RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.album_title_item, parent, false)) {

    fun bindAlbum(albumWrapper: AlbumItemWrapper) {
        itemView.albumTitle.text = albumWrapper.albumName
    }
}
