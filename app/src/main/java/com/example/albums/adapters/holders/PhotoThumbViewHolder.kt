package com.example.albums.adapters.holders

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.example.albums.R
import com.example.albums.wrapper.PhotoItemWrapper
import kotlinx.android.synthetic.main.photo_thumb_item.view.*

class PhotoThumbViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.photo_thumb_item, parent, false)
) {
    fun bindPhoto(photoWrapper: PhotoItemWrapper) {
        Glide.with(itemView.context)
            .load(photoWrapper.thumbUrl)
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(itemView.photoThumb)
    }
}