package com.example.albums.wrapper

data class PhotoItemWrapper(val thumbUrl: String) : BaseGalleryItemWrapper {
    override val itemType = BaseGalleryItemWrapper.ItemType.PHOTO_THUMB
}
