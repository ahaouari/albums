package com.example.albums.wrapper

data class AlbumItemWrapper(val albumName: String) : BaseGalleryItemWrapper {
    override val itemType = BaseGalleryItemWrapper.ItemType.ALBUM_TITLE
}
