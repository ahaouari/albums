package com.example.albums.wrapper

interface BaseGalleryItemWrapper {

    val itemType: ItemType

    enum class ItemType {
        ALBUM_TITLE,
        PHOTO_THUMB,
        NONE,
    }

}
