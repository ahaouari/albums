package com.example.albums.wrapper

class GalleryWrapper {

    var items: List<BaseGalleryItemWrapper>? = null
    var throwable: Throwable? = null
    var isLoading: Boolean = false

    constructor() {
        this.items = arrayListOf()
    }

    constructor(items: List<BaseGalleryItemWrapper>?) {
        this.items = items
    }

    constructor(throwable: Throwable) {
        this.throwable = throwable
        this.items = arrayListOf()
        this.isLoading = false
    }

    constructor(isLoading: Boolean) {
        this.isLoading = isLoading
    }
}